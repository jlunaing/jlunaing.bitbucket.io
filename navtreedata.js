/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mechatronics Portfolio", "index.html", [
    [ "0x01: Pushbutton-Controlled LED", "writeup_0x01_led.html", [
      [ "Overview", "writeup_0x01_led.html#sec_led_01", null ],
      [ "Finite-State Machine", "writeup_0x01_led.html#sec_led_02", null ],
      [ "Demo", "writeup_0x01_led.html#sec_led_03", null ]
    ] ],
    [ "0x02: Incremental Encoders", "writeup_0x02_encoder.html", [
      [ "Overview", "writeup_0x02_encoder.html#sec_enc_01", null ],
      [ "Background", "writeup_0x02_encoder.html#sec_enc_02", null ],
      [ "Software", "writeup_0x02_encoder.html#sec_enc_03", null ],
      [ "Task Diagram and Finite-State Machine", "writeup_0x02_encoder.html#sec_enc_04", null ],
      [ "Hardware", "writeup_0x02_encoder.html#sec_enc_05", null ]
    ] ],
    [ "0x03: DC Motor Control", "writeup_0x03_motor.html", [
      [ "Overview", "writeup_0x03_motor.html#sec_mot_01", null ],
      [ "Background", "writeup_0x03_motor.html#sec_mot_02", [
        [ "Spinning a Motor", "writeup_0x03_motor.html#motor", null ],
        [ "Motor Driver", "writeup_0x03_motor.html#motor_drv", null ]
      ] ],
      [ "Software", "writeup_0x03_motor.html#sec_mot_03", null ],
      [ "Testing", "writeup_0x03_motor.html#sec_mot_04", null ]
    ] ],
    [ "0x04: Closed-Loop Motor Control", "writeup_0x04_controller.html", [
      [ "Overview", "writeup_0x04_controller.html#sec_ctrl_01", null ],
      [ "Background", "writeup_0x04_controller.html#sec_ctrl_02", null ],
      [ "Implementation", "writeup_0x04_controller.html#sec_ctrl_03", null ],
      [ "Results and Discussion", "writeup_0x04_controller.html#sec_ctrl_04", null ]
    ] ],
    [ "0x05: I2C and Inertial-Measurement Units", "writeup_0x05_imu.html", [
      [ "Overview", "writeup_0x05_imu.html#sec_imu_01", null ],
      [ "Background", "writeup_0x05_imu.html#sec_imu_02", null ]
    ] ],
    [ "0x06: Ball-Balancing Platform Project", "writeup_0x06_project.html", [
      [ "Overview", "writeup_0x06_project.html#project_01", null ],
      [ "Code Design", "writeup_0x06_project.html#project_02", null ],
      [ "Controller Design", "writeup_0x06_project.html#project_03", [
        [ "System Modeling", "writeup_0x06_project.html#controller_01", null ],
        [ "Simulation of Dynamic Model", "writeup_0x06_project.html#controller_02", null ],
        [ "Full-State Feedback", "writeup_0x06_project.html#controller_03", null ]
      ] ],
      [ "Code Implementation", "writeup_0x06_project.html#project_04", [
        [ "Resistive Touch Panel", "writeup_0x06_project.html#code_01", null ]
      ] ],
      [ "Balancing the Ball", "writeup_0x06_project.html#project_05", null ]
    ] ],
    [ "0x06: Ball-Balancing Platform Modeling", "writeup_0x06_proj_dynamics.html", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_b_n_o055_8py.html",
"classencoder__0x04_1_1_encoder.html#a2f05f0f61b3bd8eff99439953a5694f2",
"classtask__touch__panel__0x06_1_1_r_t___task.html#aca895566ca96d7ff93d1b9d9f4a3fe9a",
"functions.html",
"writeup_0x04_controller.html#sec_ctrl_01"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';