var namespaces_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "class_b_n_o055_1_1_b_n_o055.html", "class_b_n_o055_1_1_b_n_o055" ],
      [ "angles_size", "_b_n_o055_8py.html#a9398cf97d0afa500f7d43002920b62ed", null ],
      [ "CALIB_START", "_b_n_o055_8py.html#a8f026df88751918aeee290fb55b9edee", null ],
      [ "CALIB_STAT", "_b_n_o055_8py.html#a6676244df2b17bb276b3229eb1774d4c", null ],
      [ "coeff_size", "_b_n_o055_8py.html#ac9452ea144670da3be9867b3f251555d", null ],
      [ "EULER_START", "_b_n_o055_8py.html#a502cff28f9a48ee553346d3d1944141c", null ],
      [ "GYR_START", "_b_n_o055_8py.html#ae046e891a939ece8461e4912d86f5a7a", null ],
      [ "NDOF_MODE", "_b_n_o055_8py.html#a8d5c325d53d5753719aed1743e082410", null ],
      [ "status_size", "_b_n_o055_8py.html#ad124fe0254ebf373a84d2da9b1aa8597", null ],
      [ "vel_size", "_b_n_o055_8py.html#a96b7f21c2f779d88cd5ccd903da963c2", null ]
    ] ],
    [ "BNO055_0x06", null, [
      [ "BNO055", "class_b_n_o055__0x06_1_1_b_n_o055.html", "class_b_n_o055__0x06_1_1_b_n_o055" ],
      [ "main", "_b_n_o055__0x06_8py.html#a97baae41d7906c51941fd5e4e609f07c", null ],
      [ "angles_size", "_b_n_o055__0x06_8py.html#a77a0d2fb8ae2b2f3770c447a0d804e75", null ],
      [ "CALIB_START", "_b_n_o055__0x06_8py.html#a4258d48df265de20985c86d65f48b6fd", null ],
      [ "CALIB_STAT", "_b_n_o055__0x06_8py.html#a22f4c1609fba6b3e5bae765df4a39e26", null ],
      [ "coeff_size", "_b_n_o055__0x06_8py.html#aa3b280a29681344cc69dec8d9cf20dcc", null ],
      [ "EULER_START", "_b_n_o055__0x06_8py.html#ab051a8a6800436fa2fa1474627f9e830", null ],
      [ "GYR_START", "_b_n_o055__0x06_8py.html#a1796e7de4c0ba261ae5ca5786f7a106e", null ],
      [ "NDOF_MODE", "_b_n_o055__0x06_8py.html#ae1a614bc32b81eba7592e8021a09da11", null ],
      [ "status_size", "_b_n_o055__0x06_8py.html#a947c865fcbf506afbde44f91528eae90", null ],
      [ "vel_size", "_b_n_o055__0x06_8py.html#a3d099b5603ec0b9b14f3d2f052ca5400", null ]
    ] ],
    [ "controller", null, [
      [ "Controller", "classcontroller_1_1_controller.html", "classcontroller_1_1_controller" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "class_d_r_v8847_1_1_d_r_v8847.html", "class_d_r_v8847_1_1_d_r_v8847" ],
      [ "Motor", "class_d_r_v8847_1_1_motor.html", "class_d_r_v8847_1_1_motor" ]
    ] ],
    [ "DRV8847_0x04", null, [
      [ "DRV8847", "class_d_r_v8847__0x04_1_1_d_r_v8847.html", "class_d_r_v8847__0x04_1_1_d_r_v8847" ],
      [ "Motor", "class_d_r_v8847__0x04_1_1_motor.html", "class_d_r_v8847__0x04_1_1_motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1_encoder.html", "classencoder_1_1_encoder" ]
    ] ],
    [ "encoder_0x03", null, [
      [ "Encoder", "classencoder__0x03_1_1_encoder.html", "classencoder__0x03_1_1_encoder" ]
    ] ],
    [ "encoder_0x04", null, [
      [ "Encoder", "classencoder__0x04_1_1_encoder.html", "classencoder__0x04_1_1_encoder" ]
    ] ],
    [ "fibonacci", null, [
      [ "fib", "fibonacci_8py.html#aa5f976771767207315adfb4fb40504bf", null ],
      [ "idx", "fibonacci_8py.html#af53015a8765b331e9e9ad4b738f02f88", null ],
      [ "move_on", "fibonacci_8py.html#ac7e09af6b81e230b21207808d4565ea9", null ],
      [ "restart_flag", "fibonacci_8py.html#a345d70dbb69b32bb24636e2d3f8ec71e", null ]
    ] ],
    [ "led_control", null, [
      [ "onButtonPress", "led__control_8py.html#a7647a6ad9faf405f1082ddabcd7c8544", null ],
      [ "resetTimer", "led__control_8py.html#a52a6c7ab147ee263e09560f826920bdd", null ],
      [ "sawtoothWave", "led__control_8py.html#a9f3ebb903eb246f7e916961a6a7d4129", null ],
      [ "sineWave", "led__control_8py.html#a9c276d235dc9447e849080dfd9aaa59a", null ],
      [ "squareWave", "led__control_8py.html#a7a84431eda86ec51e7fe37f517062a4a", null ],
      [ "turnOffLED", "led__control_8py.html#a922e5e46b190629e4d6da938141f61f2", null ],
      [ "updateTimer", "led__control_8py.html#a345979a3beb19785dae18970186f2b97", null ],
      [ "buttonFlag", "led__control_8py.html#a677630c84f22c30a70082c01aa877e74", null ],
      [ "ButtonInt", "led__control_8py.html#a59d515d1adb1e5a61d614622c6e7f6e7", null ],
      [ "pinA5", "led__control_8py.html#ae59781e4dea480f6988fd8afc0ad8e40", null ],
      [ "pinC13", "led__control_8py.html#ac9729fdf564000871294186c6f902ef5", null ],
      [ "run", "led__control_8py.html#a201f16173059a6445ce5cafa974e2217", null ],
      [ "state", "led__control_8py.html#a19f0162381dbf5ab68e777008fff60ed", null ],
      [ "t2ch1", "led__control_8py.html#a639e0772e5741e211a3ab707c8979b1b", null ],
      [ "tim2", "led__control_8py.html#a512a33e87842ac0fa1b7312ef7302dce", null ]
    ] ],
    [ "main_0x02", null, [
      [ "EncoderTaskPer", "main__0x02_8py.html#ac1554ae3b7c704bb6ff04d1a97e821cf", null ],
      [ "encTuple", "main__0x02_8py.html#a41003516ed87b0ed20e42a9d0537327a", null ],
      [ "lab2_encoderTask", "main__0x02_8py.html#a1663f82075a9d3815661f796680aa86f", null ],
      [ "lab2_userTask", "main__0x02_8py.html#a74c09a0358e27fef7af0296845d8e869", null ],
      [ "setZero", "main__0x02_8py.html#af53620861213e182661efb0d0eae59e3", null ],
      [ "UserIntTaskPer", "main__0x02_8py.html#ae2b1e5307a0c87a4ed2b32011109c4aa", null ]
    ] ],
    [ "main_0x03", null, [
      [ "main", "main__0x03_8py.html#a88b92132ad76a2a089a093eaf0f0d344", null ]
    ] ],
    [ "main_0x04", null, [
      [ "main", "main__0x04_8py.html#aba81e8ed42e4f07651c29df8232469da", null ]
    ] ],
    [ "main_0x05", "namespacemain__0x05.html", [
      [ "BAUDRATE", "namespacemain__0x05.html#a42782dae53ef2303bb82e36ebe4822fb", null ],
      [ "i2c", "namespacemain__0x05.html#a6317e40fcebcdd104d7089de639e5318", null ],
      [ "imu_drv", "namespacemain__0x05.html#ac2a19f291e1e6a0581541d96e9e84897", null ],
      [ "is_calib", "namespacemain__0x05.html#ac9d04112490475f879e922dc2ad42649", null ],
      [ "status", "namespacemain__0x05.html#accbb464e3fb30236cc81c0f8e5b0a04c", null ]
    ] ],
    [ "main_0x06", null, [
      [ "main", "main__0x06_8py.html#a934eeba5ec09e2219bb8eccfa2b17bca", null ]
    ] ],
    [ "motor_0x06", null, [
      [ "Motor", "classmotor__0x06_1_1_motor.html", "classmotor__0x06_1_1_motor" ],
      [ "main", "motor__0x06_8py.html#a826f9926a3640b4fc9455f6966b992a0", null ]
    ] ],
    [ "shares_0x03", null, [
      [ "Queue", "classshares__0x03_1_1_queue.html", "classshares__0x03_1_1_queue" ],
      [ "Share", "classshares__0x03_1_1_share.html", "classshares__0x03_1_1_share" ]
    ] ],
    [ "shares_0x04", null, [
      [ "Queue", "classshares__0x04_1_1_queue.html", "classshares__0x04_1_1_queue" ],
      [ "Share", "classshares__0x04_1_1_share.html", "classshares__0x04_1_1_share" ]
    ] ],
    [ "shares_0x06", null, [
      [ "Queue", "classshares__0x06_1_1_queue.html", "classshares__0x06_1_1_queue" ],
      [ "Share", "classshares__0x06_1_1_share.html", "classshares__0x06_1_1_share" ]
    ] ],
    [ "task_controller_0x06", null, [
      [ "Task_Control", "classtask__controller__0x06_1_1_task___control.html", "classtask__controller__0x06_1_1_task___control" ],
      [ "K_T", "task__controller__0x06_8py.html#a3bcd3eb4a11bf41cd2ac1baf5138a96b", null ],
      [ "pin_IN1", "task__controller__0x06_8py.html#ab291b8fd1655e14a885ee101865713a1", null ],
      [ "pin_IN2", "task__controller__0x06_8py.html#a177c27756a55078a7021c66fdca0089c", null ],
      [ "pin_IN3", "task__controller__0x06_8py.html#aad164a732ddfba0e4038bf0978203741", null ],
      [ "pin_IN4", "task__controller__0x06_8py.html#ae86a009e3ebb4a4bb709ea7a491b5b05", null ],
      [ "R_OHM", "task__controller__0x06_8py.html#aea72d81dc29afb99ed4543fb55d63add", null ],
      [ "V_DC", "task__controller__0x06_8py.html#a0fc18247ddd974db8caa45a53d781e56", null ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1_task___encoder.html", "classtask__encoder_1_1_task___encoder" ],
      [ "pinA", "task__encoder_8py.html#a593533239f3ffea1ee73d303115c6e86", null ],
      [ "pinB", "task__encoder_8py.html#a04fe5241cb6af0a7f34543873bf75c4f", null ]
    ] ],
    [ "task_encoder_0x03", null, [
      [ "Task_Encoder", "classtask__encoder__0x03_1_1_task___encoder.html", "classtask__encoder__0x03_1_1_task___encoder" ],
      [ "motor_info", "task__encoder__0x03_8py.html#a4dd5e3f5b71c6aad183af8d7cab6aaa7", null ],
      [ "pin1_ENC1", "task__encoder__0x03_8py.html#a6fdf72e08cb05ee92c5e2eabf1eda1c7", null ],
      [ "pin1_ENC2", "task__encoder__0x03_8py.html#a78c97f037a7fcdb607bb8de2d0ef21cc", null ],
      [ "pin2_ENC1", "task__encoder__0x03_8py.html#a005535a4b68249b777da24061206655c", null ],
      [ "pin2_ENC2", "task__encoder__0x03_8py.html#a48ed368a57aafdf7f4f8a99473183632", null ],
      [ "tick2rad", "task__encoder__0x03_8py.html#a625ebcc4a7a172113cd467caa4fad9b7", null ]
    ] ],
    [ "task_encoder_0x04", null, [
      [ "Task_Encoder", "classtask__encoder__0x04_1_1_task___encoder.html", "classtask__encoder__0x04_1_1_task___encoder" ]
    ] ],
    [ "task_IMU_0x06", null, [
      [ "Task_IMU", "classtask___i_m_u__0x06_1_1_task___i_m_u.html", "classtask___i_m_u__0x06_1_1_task___i_m_u" ]
    ] ],
    [ "task_motor_0x03", null, [
      [ "Task_Motor", "classtask__motor__0x03_1_1_task___motor.html", "classtask__motor__0x03_1_1_task___motor" ],
      [ "motor_info", "task__motor__0x03_8py.html#ab08c2f7babc612ab520ff8c6f234c214", null ],
      [ "pin_IN1", "task__motor__0x03_8py.html#ade64afe1d234e5107f2b5c398af717bd", null ],
      [ "pin_IN2", "task__motor__0x03_8py.html#ad57b3e3c7baa663b0168f9909b1d2935", null ],
      [ "pin_IN3", "task__motor__0x03_8py.html#a1716d89db2f17f3cdf2c315b63b56ef3", null ],
      [ "pin_IN4", "task__motor__0x03_8py.html#aca6b80e4678ef672d20d9d2eb4a6d873", null ]
    ] ],
    [ "task_motor_0x04", null, [
      [ "Task_Motor", "classtask__motor__0x04_1_1_task___motor.html", "classtask__motor__0x04_1_1_task___motor" ],
      [ "motor_info", "task__motor__0x04_8py.html#ad3935a2888da01396cf3ce1014ed0ce2", null ],
      [ "pin_IN1", "task__motor__0x04_8py.html#aa1ae1e92a5752d203c7de6da64f3c845", null ],
      [ "pin_IN2", "task__motor__0x04_8py.html#ac6f0483ef2ef7be5db974c96836ccd6c", null ],
      [ "pin_IN3", "task__motor__0x04_8py.html#a5eea248d9c79fee9c56fe1adf31b25eb", null ],
      [ "pin_IN4", "task__motor__0x04_8py.html#a4ae1f1b5a6e1d1eb04390a2bac9586c6", null ]
    ] ],
    [ "task_touch_panel_0x06", null, [
      [ "RT_Task", "classtask__touch__panel__0x06_1_1_r_t___task.html", "classtask__touch__panel__0x06_1_1_r_t___task" ],
      [ "pin_xm", "task__touch__panel__0x06_8py.html#a30dad7a1dc6864dd3b45d66c9bd0b89c", null ],
      [ "pin_xp", "task__touch__panel__0x06_8py.html#a1738eb2b0c6349e9fef496db25c07199", null ],
      [ "pin_ym", "task__touch__panel__0x06_8py.html#a6a8258bf2a9650183f7543d9555b54f0", null ],
      [ "pin_yp", "task__touch__panel__0x06_8py.html#a7d4f50d533d32cada013372a4b525ff5", null ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1_task___user.html", "classtask__user_1_1_task___user" ],
      [ "S0_INIT", "task__user_8py.html#aa81fcfa12c47c22962892a6b53d29287", null ],
      [ "S1_WAIT_FOR_INPUT", "task__user_8py.html#abb80c8f07ef19a810bdf9931b915e4c0", null ]
    ] ],
    [ "task_user_0x03", null, [
      [ "Task_User", "classtask__user__0x03_1_1_task___user.html", "classtask__user__0x03_1_1_task___user" ],
      [ "motor_info", "task__user__0x03_8py.html#a2895589c0d4b377cd418addf8341735b", null ],
      [ "S0_INIT", "task__user__0x03_8py.html#abcc0e1a3a1896e8dc52d6a8f2b6e0717", null ],
      [ "S1_WAIT_FOR_INPUT", "task__user__0x03_8py.html#a686902d767a49f0763df987500518fa1", null ],
      [ "serialComm", "task__user__0x03_8py.html#a8ae99666e16700ec086d58b9e863a434", null ]
    ] ],
    [ "task_user_0x04", null, [
      [ "Task_User", "classtask__user__0x04_1_1_task___user.html", "classtask__user__0x04_1_1_task___user" ],
      [ "comm_reader", "task__user__0x04_8py.html#a5f26dc088f59688caa2461b3da12c81c", null ],
      [ "motor_info", "task__user__0x04_8py.html#adfbd8e02d682ff661075977c83390f78", null ],
      [ "pid_prints", "task__user__0x04_8py.html#a2c310ba8a72d6f519aac5733902ba3d8", null ],
      [ "record_time", "task__user__0x04_8py.html#af6cbeb1da70e4303425aeb89d36b0314", null ],
      [ "S0_INIT", "task__user__0x04_8py.html#a4d66adb7161c94caa499b37e3bfec872", null ],
      [ "S1_WAIT_FOR_INPUT", "task__user__0x04_8py.html#a445ac9c78985869e62dd541d9084551f", null ],
      [ "S2_PROMPT", "task__user__0x04_8py.html#ad1f6b0cf9738aa7320e0b7ae61c0c539", null ],
      [ "S3_TIME_DELAY", "task__user__0x04_8py.html#a62dce0a1c6bfc341dc572d1164de0c91", null ],
      [ "step_time", "task__user__0x04_8py.html#a8f85ed48ee50d11d2f97fa34e40f0daf", null ]
    ] ],
    [ "task_user_0x06", null, [
      [ "Task_User", "classtask__user__0x06_1_1_task___user.html", "classtask__user__0x06_1_1_task___user" ],
      [ "S0_INIT", "task__user__0x06_8py.html#a993ffc9268fbdc312fb349baae787f4c", null ],
      [ "S1_WAIT_FOR_INPUT", "task__user__0x06_8py.html#a1bc3249e258a6bc54e01e1cd175813c2", null ],
      [ "S2_PROMPT", "task__user__0x06_8py.html#a90519da5e9255ce7217a9ee2333c0295", null ],
      [ "S3_TIME_DELAY", "task__user__0x06_8py.html#a339b8ca790427ea6e836ef3f45c94462", null ]
    ] ],
    [ "touch_panel", null, [
      [ "Touch_Panel", "classtouch__panel_1_1_touch___panel.html", "classtouch__panel_1_1_touch___panel" ],
      [ "main", "touch__panel_8py.html#affdd7fe96091673d41fec17b3e9c1789", null ],
      [ "ADC_count", "touch__panel_8py.html#a853183da937961509b9d128a43172eda", null ],
      [ "Kxx", "touch__panel_8py.html#a1a3df922d818c571bd500e405513c279", null ],
      [ "Kxy", "touch__panel_8py.html#a208ebd41528ce37c5f9c3ce5f75524e6", null ],
      [ "Kyx", "touch__panel_8py.html#a19a41c454fbc253ea96201e1a2944ac1", null ],
      [ "Kyy", "touch__panel_8py.html#ad7369d91ddd5b3db23da259239d5710b", null ],
      [ "length_x", "touch__panel_8py.html#a7cb072cfce4b7cd07a0fc57b93f23ec2", null ],
      [ "width_y", "touch__panel_8py.html#abb00afbe854bab7711b73eae2af2034e", null ],
      [ "x0", "touch__panel_8py.html#a6d3d020f9bccf09a43aca77e569b3a1c", null ],
      [ "y0", "touch__panel_8py.html#a85228dee8026e1ae96fccb410c528c98", null ]
    ] ],
    [ "writeup_0x06_project_dynamics", "namespacewriteup__0x06__project__dynamics.html", null ]
];