var dir_2d10b1ac8ab35f9a434d67a9d6d2c6a0 =
[
    [ "BNO055_0x06.py", "_b_n_o055__0x06_8py.html", "_b_n_o055__0x06_8py" ],
    [ "main_0x06.py", "main__0x06_8py.html", "main__0x06_8py" ],
    [ "motor_0x06.py", "motor__0x06_8py.html", "motor__0x06_8py" ],
    [ "shares_0x06.py", "shares__0x06_8py.html", [
      [ "shares_0x06.Share", "classshares__0x06_1_1_share.html", "classshares__0x06_1_1_share" ],
      [ "shares_0x06.Queue", "classshares__0x06_1_1_queue.html", "classshares__0x06_1_1_queue" ]
    ] ],
    [ "task_controller_0x06.py", "task__controller__0x06_8py.html", "task__controller__0x06_8py" ],
    [ "task_IMU_0x06.py", "task___i_m_u__0x06_8py.html", [
      [ "task_IMU_0x06.Task_IMU", "classtask___i_m_u__0x06_1_1_task___i_m_u.html", "classtask___i_m_u__0x06_1_1_task___i_m_u" ]
    ] ],
    [ "task_touch_panel_0x06.py", "task__touch__panel__0x06_8py.html", "task__touch__panel__0x06_8py" ],
    [ "task_user_0x06.py", "task__user__0x06_8py.html", "task__user__0x06_8py" ],
    [ "touch_panel.py", "touch__panel_8py.html", "touch__panel_8py" ]
];