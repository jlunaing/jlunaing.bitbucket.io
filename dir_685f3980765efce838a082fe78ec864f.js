var dir_685f3980765efce838a082fe78ec864f =
[
    [ "DRV8847.py", "_d_r_v8847_8py.html", [
      [ "DRV8847.DRV8847", "class_d_r_v8847_1_1_d_r_v8847.html", "class_d_r_v8847_1_1_d_r_v8847" ],
      [ "DRV8847.Motor", "class_d_r_v8847_1_1_motor.html", "class_d_r_v8847_1_1_motor" ]
    ] ],
    [ "encoder_0x03.py", "encoder__0x03_8py.html", [
      [ "encoder_0x03.Encoder", "classencoder__0x03_1_1_encoder.html", "classencoder__0x03_1_1_encoder" ]
    ] ],
    [ "main_0x03.py", "main__0x03_8py.html", "main__0x03_8py" ],
    [ "shares_0x03.py", "shares__0x03_8py.html", [
      [ "shares_0x03.Share", "classshares__0x03_1_1_share.html", "classshares__0x03_1_1_share" ],
      [ "shares_0x03.Queue", "classshares__0x03_1_1_queue.html", "classshares__0x03_1_1_queue" ]
    ] ],
    [ "task_encoder_0x03.py", "task__encoder__0x03_8py.html", "task__encoder__0x03_8py" ],
    [ "task_motor_0x03.py", "task__motor__0x03_8py.html", "task__motor__0x03_8py" ],
    [ "task_user_0x03.py", "task__user__0x03_8py.html", "task__user__0x03_8py" ]
];