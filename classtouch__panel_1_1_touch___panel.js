var classtouch__panel_1_1_touch___panel =
[
    [ "__init__", "classtouch__panel_1_1_touch___panel.html#a6a7cb2f4a7db50d5d55567d4b8dfdc32", null ],
    [ "calibrate", "classtouch__panel_1_1_touch___panel.html#a868de9972436d4eb326ae3509305af9b", null ],
    [ "scan_all", "classtouch__panel_1_1_touch___panel.html#a0d9f21456e91a0968530d0baed0d025b", null ],
    [ "x_scan", "classtouch__panel_1_1_touch___panel.html#aa69d48f4ca85f9abea13e9e2517dcdd2", null ],
    [ "xy_scan", "classtouch__panel_1_1_touch___panel.html#aec2d91ad79dca4786bbf7437921ed544", null ],
    [ "y_scan", "classtouch__panel_1_1_touch___panel.html#a9f4617fd203309f2e5099e95792257e6", null ],
    [ "z_scan", "classtouch__panel_1_1_touch___panel.html#a77a75a122c4c218dff85cd6c7764eb81", null ],
    [ "pin_XM", "classtouch__panel_1_1_touch___panel.html#aecac733e63669ef18794e717b55ed677", null ],
    [ "pin_XP", "classtouch__panel_1_1_touch___panel.html#a792559e9ce894d13a63cb28f7db39544", null ],
    [ "pin_YM", "classtouch__panel_1_1_touch___panel.html#a1408d0813094ce09dc4837ac339a2dde", null ],
    [ "pin_YP", "classtouch__panel_1_1_touch___panel.html#a83b05a8db6c07ea8fd4fb00149491a9d", null ]
];