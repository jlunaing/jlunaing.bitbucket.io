var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "class_b_n_o055_1_1_b_n_o055.html", "class_b_n_o055_1_1_b_n_o055" ]
    ] ],
    [ "BNO055_0x06", null, [
      [ "BNO055", "class_b_n_o055__0x06_1_1_b_n_o055.html", "class_b_n_o055__0x06_1_1_b_n_o055" ]
    ] ],
    [ "controller", null, [
      [ "Controller", "classcontroller_1_1_controller.html", "classcontroller_1_1_controller" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "class_d_r_v8847_1_1_d_r_v8847.html", "class_d_r_v8847_1_1_d_r_v8847" ],
      [ "Motor", "class_d_r_v8847_1_1_motor.html", "class_d_r_v8847_1_1_motor" ]
    ] ],
    [ "DRV8847_0x04", null, [
      [ "DRV8847", "class_d_r_v8847__0x04_1_1_d_r_v8847.html", "class_d_r_v8847__0x04_1_1_d_r_v8847" ],
      [ "Motor", "class_d_r_v8847__0x04_1_1_motor.html", "class_d_r_v8847__0x04_1_1_motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1_encoder.html", "classencoder_1_1_encoder" ]
    ] ],
    [ "encoder_0x03", null, [
      [ "Encoder", "classencoder__0x03_1_1_encoder.html", "classencoder__0x03_1_1_encoder" ]
    ] ],
    [ "encoder_0x04", null, [
      [ "Encoder", "classencoder__0x04_1_1_encoder.html", "classencoder__0x04_1_1_encoder" ]
    ] ],
    [ "motor_0x06", null, [
      [ "Motor", "classmotor__0x06_1_1_motor.html", "classmotor__0x06_1_1_motor" ]
    ] ],
    [ "shares_0x03", null, [
      [ "Queue", "classshares__0x03_1_1_queue.html", "classshares__0x03_1_1_queue" ],
      [ "Share", "classshares__0x03_1_1_share.html", "classshares__0x03_1_1_share" ]
    ] ],
    [ "shares_0x04", null, [
      [ "Queue", "classshares__0x04_1_1_queue.html", "classshares__0x04_1_1_queue" ],
      [ "Share", "classshares__0x04_1_1_share.html", "classshares__0x04_1_1_share" ]
    ] ],
    [ "shares_0x06", null, [
      [ "Queue", "classshares__0x06_1_1_queue.html", "classshares__0x06_1_1_queue" ],
      [ "Share", "classshares__0x06_1_1_share.html", "classshares__0x06_1_1_share" ]
    ] ],
    [ "task_controller_0x06", null, [
      [ "Task_Control", "classtask__controller__0x06_1_1_task___control.html", "classtask__controller__0x06_1_1_task___control" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1_task___encoder.html", "classtask__encoder_1_1_task___encoder" ]
    ] ],
    [ "task_encoder_0x03", null, [
      [ "Task_Encoder", "classtask__encoder__0x03_1_1_task___encoder.html", "classtask__encoder__0x03_1_1_task___encoder" ]
    ] ],
    [ "task_encoder_0x04", null, [
      [ "Task_Encoder", "classtask__encoder__0x04_1_1_task___encoder.html", "classtask__encoder__0x04_1_1_task___encoder" ]
    ] ],
    [ "task_IMU_0x06", null, [
      [ "Task_IMU", "classtask___i_m_u__0x06_1_1_task___i_m_u.html", "classtask___i_m_u__0x06_1_1_task___i_m_u" ]
    ] ],
    [ "task_motor_0x03", null, [
      [ "Task_Motor", "classtask__motor__0x03_1_1_task___motor.html", "classtask__motor__0x03_1_1_task___motor" ]
    ] ],
    [ "task_motor_0x04", null, [
      [ "Task_Motor", "classtask__motor__0x04_1_1_task___motor.html", "classtask__motor__0x04_1_1_task___motor" ]
    ] ],
    [ "task_touch_panel_0x06", null, [
      [ "RT_Task", "classtask__touch__panel__0x06_1_1_r_t___task.html", "classtask__touch__panel__0x06_1_1_r_t___task" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1_task___user.html", "classtask__user_1_1_task___user" ]
    ] ],
    [ "task_user_0x03", null, [
      [ "Task_User", "classtask__user__0x03_1_1_task___user.html", "classtask__user__0x03_1_1_task___user" ]
    ] ],
    [ "task_user_0x04", null, [
      [ "Task_User", "classtask__user__0x04_1_1_task___user.html", "classtask__user__0x04_1_1_task___user" ]
    ] ],
    [ "task_user_0x06", null, [
      [ "Task_User", "classtask__user__0x06_1_1_task___user.html", "classtask__user__0x06_1_1_task___user" ]
    ] ],
    [ "touch_panel", null, [
      [ "Touch_Panel", "classtouch__panel_1_1_touch___panel.html", "classtouch__panel_1_1_touch___panel" ]
    ] ]
];