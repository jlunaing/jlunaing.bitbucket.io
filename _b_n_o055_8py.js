var _b_n_o055_8py =
[
    [ "BNO055.BNO055", "class_b_n_o055_1_1_b_n_o055.html", "class_b_n_o055_1_1_b_n_o055" ],
    [ "angles_size", "_b_n_o055_8py.html#a9398cf97d0afa500f7d43002920b62ed", null ],
    [ "CALIB_START", "_b_n_o055_8py.html#a8f026df88751918aeee290fb55b9edee", null ],
    [ "CALIB_STAT", "_b_n_o055_8py.html#a6676244df2b17bb276b3229eb1774d4c", null ],
    [ "coeff_size", "_b_n_o055_8py.html#ac9452ea144670da3be9867b3f251555d", null ],
    [ "EULER_START", "_b_n_o055_8py.html#a502cff28f9a48ee553346d3d1944141c", null ],
    [ "GYR_START", "_b_n_o055_8py.html#ae046e891a939ece8461e4912d86f5a7a", null ],
    [ "NDOF_MODE", "_b_n_o055_8py.html#a8d5c325d53d5753719aed1743e082410", null ],
    [ "status_size", "_b_n_o055_8py.html#ad124fe0254ebf373a84d2da9b1aa8597", null ],
    [ "vel_size", "_b_n_o055_8py.html#a96b7f21c2f779d88cd5ccd903da963c2", null ]
];