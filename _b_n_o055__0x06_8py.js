var _b_n_o055__0x06_8py =
[
    [ "BNO055_0x06.BNO055", "class_b_n_o055__0x06_1_1_b_n_o055.html", "class_b_n_o055__0x06_1_1_b_n_o055" ],
    [ "main", "_b_n_o055__0x06_8py.html#a97baae41d7906c51941fd5e4e609f07c", null ],
    [ "angles_size", "_b_n_o055__0x06_8py.html#a77a0d2fb8ae2b2f3770c447a0d804e75", null ],
    [ "CALIB_START", "_b_n_o055__0x06_8py.html#a4258d48df265de20985c86d65f48b6fd", null ],
    [ "CALIB_STAT", "_b_n_o055__0x06_8py.html#a22f4c1609fba6b3e5bae765df4a39e26", null ],
    [ "coeff_size", "_b_n_o055__0x06_8py.html#aa3b280a29681344cc69dec8d9cf20dcc", null ],
    [ "EULER_START", "_b_n_o055__0x06_8py.html#ab051a8a6800436fa2fa1474627f9e830", null ],
    [ "GYR_START", "_b_n_o055__0x06_8py.html#a1796e7de4c0ba261ae5ca5786f7a106e", null ],
    [ "NDOF_MODE", "_b_n_o055__0x06_8py.html#ae1a614bc32b81eba7592e8021a09da11", null ],
    [ "status_size", "_b_n_o055__0x06_8py.html#a947c865fcbf506afbde44f91528eae90", null ],
    [ "vel_size", "_b_n_o055__0x06_8py.html#a3d099b5603ec0b9b14f3d2f052ca5400", null ]
];