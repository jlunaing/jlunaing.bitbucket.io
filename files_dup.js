var files_dup =
[
    [ "0x00_Fibonacci_Calculator", "dir_f7fdd8551f961f65bfce3f09b1b75b1d.html", "dir_f7fdd8551f961f65bfce3f09b1b75b1d" ],
    [ "0x01_Pushbutton_Controlled_LED", "dir_4ed169f18e19a5ce2819be5e82bad57c.html", "dir_4ed169f18e19a5ce2819be5e82bad57c" ],
    [ "0x02_Encoder_Control", "dir_98681db907eea7e1fed02ad53af0a5d7.html", "dir_98681db907eea7e1fed02ad53af0a5d7" ],
    [ "0x03_DC_Motor_Control", "dir_685f3980765efce838a082fe78ec864f.html", "dir_685f3980765efce838a082fe78ec864f" ],
    [ "0x04_Closed_Loop_Control", "dir_a868fec8d80a96ef92c426a16ae47074.html", "dir_a868fec8d80a96ef92c426a16ae47074" ],
    [ "0x05_I2C_with_IMU", "dir_e4bacf477dc5ae40ae6a310f85c23193.html", "dir_e4bacf477dc5ae40ae6a310f85c23193" ],
    [ "0x06_Balancing_Platform", "dir_2d10b1ac8ab35f9a434d67a9d6d2c6a0.html", "dir_2d10b1ac8ab35f9a434d67a9d6d2c6a0" ],
    [ "0xFF_Portfolio", "dir_f4f0e5d9f8ed8d4bf09d5937e8e8ea33.html", "dir_f4f0e5d9f8ed8d4bf09d5937e8e8ea33" ]
];