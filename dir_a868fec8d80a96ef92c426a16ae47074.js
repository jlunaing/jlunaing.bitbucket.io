var dir_a868fec8d80a96ef92c426a16ae47074 =
[
    [ "controller.py", "controller_8py.html", [
      [ "controller.Controller", "classcontroller_1_1_controller.html", "classcontroller_1_1_controller" ]
    ] ],
    [ "DRV8847_0x04.py", "_d_r_v8847__0x04_8py.html", [
      [ "DRV8847_0x04.DRV8847", "class_d_r_v8847__0x04_1_1_d_r_v8847.html", "class_d_r_v8847__0x04_1_1_d_r_v8847" ],
      [ "DRV8847_0x04.Motor", "class_d_r_v8847__0x04_1_1_motor.html", "class_d_r_v8847__0x04_1_1_motor" ]
    ] ],
    [ "encoder_0x04.py", "encoder__0x04_8py.html", [
      [ "encoder_0x04.Encoder", "classencoder__0x04_1_1_encoder.html", "classencoder__0x04_1_1_encoder" ]
    ] ],
    [ "main_0x04.py", "main__0x04_8py.html", "main__0x04_8py" ],
    [ "shares_0x04.py", "shares__0x04_8py.html", [
      [ "shares_0x04.Share", "classshares__0x04_1_1_share.html", "classshares__0x04_1_1_share" ],
      [ "shares_0x04.Queue", "classshares__0x04_1_1_queue.html", "classshares__0x04_1_1_queue" ]
    ] ],
    [ "task_motor_0x04.py", "task__motor__0x04_8py.html", "task__motor__0x04_8py" ],
    [ "task_user_0x04.py", "task__user__0x04_8py.html", "task__user__0x04_8py" ]
];