var class_b_n_o055__0x06_1_1_b_n_o055 =
[
    [ "__init__", "class_b_n_o055__0x06_1_1_b_n_o055.html#a02b2bf59511c9a46e48b22b5006aceed", null ],
    [ "calibrate", "class_b_n_o055__0x06_1_1_b_n_o055.html#abfdc099c9eb548e91f7462164039dec1", null ],
    [ "get_calib_coeff", "class_b_n_o055__0x06_1_1_b_n_o055.html#a36ef80cb54db9a725e2bf498a2254d31", null ],
    [ "get_calib_stat", "class_b_n_o055__0x06_1_1_b_n_o055.html#a5b0b974fb96e4f13ce2776b09ed2878f", null ],
    [ "print_cal", "class_b_n_o055__0x06_1_1_b_n_o055.html#ae2f51c5d80b7cd4ab91359e48d02c32c", null ],
    [ "read_euler_angles", "class_b_n_o055__0x06_1_1_b_n_o055.html#a2371f40cc59320a8f8dcfb851d73a800", null ],
    [ "read_velocity", "class_b_n_o055__0x06_1_1_b_n_o055.html#a543d4aa65e5d0a765b8b0aa75a723849", null ],
    [ "set_mode", "class_b_n_o055__0x06_1_1_b_n_o055.html#a14b5392a423cfb39fd72a6baffb1e152", null ],
    [ "turn_off", "class_b_n_o055__0x06_1_1_b_n_o055.html#a5c92018d0a45aec58ae1498075a2f5ae", null ],
    [ "write_calib_coeff", "class_b_n_o055__0x06_1_1_b_n_o055.html#a7b6ae9f78a5c8a9d8e333189a638a291", null ],
    [ "address", "class_b_n_o055__0x06_1_1_b_n_o055.html#a07ef0f1c2825273e01287607447d8616", null ],
    [ "buf_angles", "class_b_n_o055__0x06_1_1_b_n_o055.html#a6d9a684a40b9c7002acee9fe627aab45", null ],
    [ "buf_coeff", "class_b_n_o055__0x06_1_1_b_n_o055.html#a2fd75f87bdfbd26c81366e6313a28ce2", null ],
    [ "buf_status", "class_b_n_o055__0x06_1_1_b_n_o055.html#adfd6f2ed2dd83b3d8efd723f9fd6d823", null ],
    [ "buf_vels", "class_b_n_o055__0x06_1_1_b_n_o055.html#a3293781ccd0c7e1e2bb610552b2d5c4b", null ],
    [ "i2c", "class_b_n_o055__0x06_1_1_b_n_o055.html#a8c33a95ef807d37b26e71aaa527a89a4", null ]
];