var classtask__controller__0x06_1_1_task___control =
[
    [ "__init__", "classtask__controller__0x06_1_1_task___control.html#ab9687bb2c0d8599a2229f9e12d16cb52", null ],
    [ "collect_data", "classtask__controller__0x06_1_1_task___control.html#a632c3f442f83f69c0cb26607d957f39b", null ],
    [ "run", "classtask__controller__0x06_1_1_task___control.html#a6081bafd250122608570a186c7a389f9", null ],
    [ "balance", "classtask__controller__0x06_1_1_task___control.html#a1b905d6467f14cc93df8a65d1f651a01", null ],
    [ "collect", "classtask__controller__0x06_1_1_task___control.html#a32b1b3e42a75675a39a8cc993d34c110", null ],
    [ "collection_period", "classtask__controller__0x06_1_1_task___control.html#a382ca5a3f8d0dbccfdb30c75061f03f8", null ],
    [ "delta_time", "classtask__controller__0x06_1_1_task___control.html#a5d08ad8035b17b73e844a8519048b23d", null ],
    [ "duty_M1", "classtask__controller__0x06_1_1_task___control.html#a942801f4ba7ce28c4ba405c8fb3a47c7", null ],
    [ "duty_M2", "classtask__controller__0x06_1_1_task___control.html#a100deabefa0e31ad22c85df10e4a94f9", null ],
    [ "IMU_queue", "classtask__controller__0x06_1_1_task___control.html#a62424bc37f5a6d4c238ea1413ace29e7", null ],
    [ "IMU_shares", "classtask__controller__0x06_1_1_task___control.html#a470f70a235e64ee7b0e687282a7d4a0f", null ],
    [ "motor_drv_1", "classtask__controller__0x06_1_1_task___control.html#ab8052f3fdb0a14a2c93465363096ae88", null ],
    [ "motor_drv_2", "classtask__controller__0x06_1_1_task___control.html#aa5907758a98c8b259df975818d7650ab", null ],
    [ "motor_period", "classtask__controller__0x06_1_1_task___control.html#ab7cdf5ef1b767c1df5b246b3ee189349", null ],
    [ "motor_queue", "classtask__controller__0x06_1_1_task___control.html#adc402f3c20185ea2a820bf9074fc3c16", null ],
    [ "next_collect", "classtask__controller__0x06_1_1_task___control.html#a84e4bbd7c1e4cf39be54e2a1be6bb4eb", null ],
    [ "next_time", "classtask__controller__0x06_1_1_task___control.html#a31b8449e8f06390e46d6f969511227b7", null ],
    [ "RTP_queue", "classtask__controller__0x06_1_1_task___control.html#a9e62b824753d5e25561371c3a6c9ae40", null ],
    [ "RTP_shares", "classtask__controller__0x06_1_1_task___control.html#a0d3a2f8b93d82111035669ce01d01dbf", null ],
    [ "start_time", "classtask__controller__0x06_1_1_task___control.html#a635815ecdb9658d335b85a794caec8c3", null ],
    [ "stop_data", "classtask__controller__0x06_1_1_task___control.html#af37c6f29d6d98f2729dbe4e7dba718a0", null ]
];